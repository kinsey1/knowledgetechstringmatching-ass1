Readme for string_matchers.py - Kinsey Reeves 695705

This python script was written and runs in Python 3.5 or greater.
The programs runs with several command line arguments as follows

python string_matchers.py  <dictionary_file> <misspelt file> <correct_file> <thresh> <method>


dictionary_file - the dictionary input file
misspelt_file - the misspelt input file
correct_file - the correct words input file. this will be the same length as the misspelt file
thresh - the distance threshold below which words are considered
method - either 'l' or 'e'

To change other parameters, the function defaults runner_lev and runner_edit must be edited from where
they are called (at the end of the file)