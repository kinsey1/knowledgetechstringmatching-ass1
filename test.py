# import random

# a = open("data/misspell.txt", 'r')
# b = open("data/correct.txt", 'r')

# out = open("data/misspell_sample.txt", 'w')
# out_c = open("data/correct_sample.txt", 'w')


# for m, c in zip(a,b):
#     m = m.strip('\n')
#     c = c.strip('\n')
#     r_int = random.randint(0,10)
#     if(r_int>7):
#         out.write(m + '\n')
#         out_c.write(c + '\n')
        


def get_ngrams(a, n, weight_start = False):
        a = '%'+a+'%'
        grams = []
        for i in range(0,len(a)-1):
            grams.append(a[i:i+n])
        return grams

def ngram_dist(a,b):
    '''
    Takes in two pre-comuted
    lists a,b of ngrams and computes 
    their 'distance'
    '''
    a = get_ngrams(a,2 )
    b = get_ngrams(b,2)
    dist = len(a) + len(b) - 2*len(set.intersection(set(a), set(b)))
    return dist

print(ngram_dist("hello", "helol"))

