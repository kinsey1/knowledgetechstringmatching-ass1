import numpy as np
import sys
import time

'''
Project 1 - Knowledge Technologies. Kinsey Reeves 695705
Implementation of Levenschtein distance and Editex Distance
to match strings in a mispelt file to ones in a dictionary file.

'''


def runner_lev(dictionary_words, mispelt_words, dist_thresh = 3, 
    first_letters = 0, skip_zero = True, num_lets = 1):
    '''
     inputs:
    dictionary_words = a list of english words to search possible guesses in
    mispelt_words = list of words to find the correction spelling of
    dist_thresh = distance under which words will be considered
    first_letters = if we should only consider words with a first letter match
    skip_zero = skip zero distance values i.e. equal words
    num_lets = the number of first letters that must be equal
    '''

    def leven_dist(str_a, str_b):
        '''
        Computes the global edit distance/Levenshetein between two strings
        parameters set to :
            match = 1
            insert/delete/replace = -1
        '''
        len_a = len(str_a)
        len_b = len(str_b)
        #We want len(a) < len(b)
        if(len_a > len_b):
            str_a, str_b = str_b, str_a
            len_a, len_b = len_b, len_a
        
        current = range(len_a + 1)
        for i in range(1,len_b+1):
            prev = current
            current = [i] + [0]*len_b
            for j in range(1,len_a+1):
                insert = prev[j]+1
                delete = current[j-1] + 1
                change = prev[j-1]
                if str_a[j-1] != str_b[i-1]:
                    change+=1
                current[j] = min(insert, delete, change)

        return current[len_a]

    count = 0
    output = []
    
    for mispelt in mispelt_words:
        best_words = []
        best_dist = dist_thresh
        best_word = ''
        for dict_word in dictionary_words:
            #print(dict_word)
            if(first_letters):
                if(dict_word[0:num_lets]!=mispelt[0:num_lets]):
                    continue
            dist = leven_dist(mispelt, dict_word)
            if(skip_zero and dist==0):
                    continue
            if(dist==best_dist):
                    best_words.append(dict_word)
            if(dist<best_dist):
                if(best_dist>1):
                    best_dist = dist
                    best_word = dict_word
                    best_words = []
                    best_words.append(best_word)
                if(best_dist==1):
                    best_words.append(dict_word)
            
        output.append((mispelt,best_words))
    
    return output


def runer_editex(dictionary_words, mispelt_words, dist_thresh = 5, first_letters = True, num_lets = 2, skip_zero = True):
    """
    Based on ideas described in:

    "Phonetic String Matching: Lessons Learned from Information Retrieval"
    by Justin Zobel and Philip Dart, SIGIR 1995.

    Used implementation similar to :
    
    http://users.cecs.anu.edu.au/~Peter.Christen/Febrl/febrl-0.4.1/stringcmp.py

    inputs:
    dictionary_words = a list of english words to search possible guesses in
    mispelt_words = list of words to find the correction spelling of
    dist_thresh = distance under which words will be considered
    first_letters = if we should only consider words with a first letter match
    skip_zero = skip zero distance values i.e. equal words
    num_lets = the number of first letters that must be equal
    """
    
    match_cost, group_cost, mismatch_cost = (0, 1, 2)
    letter_groups = (frozenset('aeiouy'), frozenset('bp'), frozenset('ckq'),
                    frozenset('dt'), frozenset('lr'), frozenset('mn'),
                    frozenset('gj'), frozenset('fpv'), frozenset('sxz'),
                    frozenset('csz'))
    all_letters = frozenset('aeiouybpckqdtlrmngjfpvsxzcsz')

    def d_cost(ch1, ch2):
        """Return d(a,b) according to Zobel & Dart's definition
        """
        if ch1 != ch2 and (ch1 == 'h' or ch1 == 'w'):
            return group_cost
        return r_cost(ch1, ch2)

    def r_cost(a,b):
        if(a==b):
            return match_cost
        if a in all_letters and b in all_letters:
            for group in letter_groups:
                if a in group and b in group:
                    return group_cost
        return mismatch_cost


    def editex(str_a, str_b):
        '''
        Editex algorithm implementation based on Dart and 
        Zobels description
        '''
        len_str_a = len(str_a)
        len_str_b = len(str_b)
        d_mat = np.zeros((len_str_a+1, len_str_b+1), dtype = np.int)
        str_a = ' '+str_a
        str_b = ' '+str_b

        for i in range(1,len_str_a+1):
            d_mat[i,0] = d_mat[i-1, 0] + d_cost(str_a[i-1], str_a[i])
        for j in range(1,len_str_b+1):
            d_mat[0,j] = d_mat[0, j-1] + d_cost(str_b[j-1], str_b[j])

        for i in range(1,len_str_a+1):
            for j in range(1, len_str_b+1):
                d_mat[i,j] = min(d_mat[i-1, j] + d_cost(str_a[i-1], str_a[i]),
                                d_mat[i, j-1] + d_cost(str_b[j-1], str_b[j]),
                                d_mat[i-1, j-1] + r_cost(str_a[i], str_b[j]))
    
        return d_mat[len_str_a, len_str_b]

    output = []
    
    for mispelt in mispelt_words:
        best_words = []
        best_dist = dist_thresh
        best_word = ''
        # Iterate through all dictionary words
        for dict_word in dictionary_words:
            
            if(first_letters):
                # If the words first matching letters aren't equal
                if(dict_word[0:num_lets]!=mispelt[0:num_lets]):
                    continue

            dist = editex(mispelt, dict_word)
            if(skip_zero and dist==0):
                continue
            # Decrease the best distance better matches are found
            if(dist==best_dist):
                    best_words.append(dict_word)
            if(dist<best_dist):
                if(best_dist>1):
                    best_dist = dist
                    best_word = dict_word
                    best_words = []
                    best_words.append(best_word)
                if(best_dist==1):
                    best_words.append(dict_word)
       
        
        output.append((mispelt,best_words))
    return output



def decide_guesses(guesses_full):
    '''
    Decides the final guess by simply selecting the
    first index in the list. (Tie breaking of first 
    letters is done in the distance algorithm for 
    efficiency)
    '''
    output = []
    
    for guess in guesses_full:
        if(len(guess[1])>0):
            best_guess = guess[1][0]
        else:
            best_guess = ""
        output.append(best_guess)
    return output


def get_stats(guesses_full, correct_file, output_file, full_write = True):
    '''
    Gets the stats for 
    '''
    correct_file = open(correct_file)
    output_file = open(output_file, 'w')
    correct_words = correct_file.readlines()
    correct_words = [x.strip('\n') for x in correct_words]
    output_file.write('guess,correct\n')
    correct_guesses = 0
    for guess, correct in zip(guesses_full,correct_words):
        if(full_write):
            output_file.write(guess + ',' +  correct + '\n')
        if(guess == correct):
            correct_guesses+=1


    accuracy = correct_guesses/len(correct_words)
    print("Accuracy: " + str(accuracy))
    output_file.write(str(accuracy))

# Beginning of program

if(len(sys.argv)<5):
    print("Usage:  python string_matchers.py  <dictionary_file> \
        <misspelt file> <correct_file> <thresh> <method> ")
else:
    dict_file = sys.argv[1]
    misspell_file = sys.argv[2]
    correct_file = sys.argv[3]
    output_file = "output.txt"
    thresh = int(sys.argv[4])
    method = sys.argv[5]

#open files
dict_file = open(dict_file, 'r')
misspell_file = open(misspell_file, 'r')

mispelt_words = misspell_file.readlines()
mispelt_words = [x.strip('\n') for x in mispelt_words]
dictionary_words = []

for line in dict_file:
    dictionary_words.append(line.strip('\n'))

#Run the correct method
if(method == 'l'):
    print('Running levenschtein algorithm')
    full_guesses = runner_lev(dictionary_words, mispelt_words, 
        dist_thresh=int(thresh), first_letters=True, skip_zero= True)
elif(method == 'e'):
    print('Running editex algorithm')
    full_guesses = runer_editex(dictionary_words, mispelt_words, 
        dist_thresh=int(thresh), first_letters=False,
        skip_zero=True, num_lets = 2)
else:
    full_guesses = runer_editex(dictionary_words, mispelt_words, 
        dist_thresh=int(thresh), first_letters=False,
        skip_zero=True, num_lets = 2)

decided_guesses = decide_guesses(full_guesses)
get_stats(decided_guesses, correct_file, output_file)