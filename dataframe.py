import numpy as np
import pandas as pd
from matplotlib import pyplot


df = pd.DataFrame(np.random.rand(5,1), columns=['Col1'])
df2 = pd.DataFrame(np.random.rand(10,1), columns=['Col2'])
df = pd.concat([df, df2], ignore_index=True, axis=1) 
