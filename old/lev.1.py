def levenshtein(a,b):
    "Calculates the Levenshtein distance between a and b."
    len_a = len(a)
    len_b = len(b)
    if len_a > len_b:
        str_a,str_b = str_b,str_a
        len_a,len_b = len_b,len_a

    current = range(len_a+1)
    for i in range(1,len_b+1):
        previous, current = current, [i]+[0]*len_a
        for j in range(1,len_a+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[len_a]

print(levenshtein("hello", "helko"))