import numpy as np


def leven_dist(str_a, str_b):
    '''
    Computes the global edit distance/Levenshetein between two strings
    parameters set to :
        match = 1
        insert/delete/replace = -1
    '''
    len_a = len(str_a)
    len_b = len(str_b)
    #We want len(a) < len(b)
    if(len_a > len_b):
        str_a, str_b = str_b, str_a
        len_a, len_b = len_b, len_a
    
    current = range(len_a + 1)
    for i in range(1,len_b+1):
        prev = current
        current = [i] + [0]*len_b
        for j in range(1,len_a+1):
            insert = prev[j]+1
            delete = current[j-1] + 1
            change = prev[j-1]
            if str_a[j-1] != str_b[i-1]:
                change+=1
            current[j] = min(insert, delete, change)

    return current[len_a]


print(levenshtein("ardvark", "arkvard"))
print(leven_dist("ardvark", "arkvard"))